﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskOne
{
    public static class StringExtensions
    {
        public static IEnumerable<byte> GetDigits(this string str)
        {
            if (str.Length == 0)
                return new List<byte>();

            var digits =
                from ch in str.ToCharArray()
                where char.IsDigit(ch)
                select Convert.ToByte(ch.ToString());

            return digits;
        }
    }
}
