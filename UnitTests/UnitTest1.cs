using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskOne;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class JuniorDevelopersUnitTest
    {
        [TestMethod]
        public void TestGetDigits()
        {
            var testStrings = new string[]
            {
                "15B541SDFG",
                "65SDBa690",
                "GGG2",
                "GHASKWOQW",
                "11972141SSGA",
                ""
            };

            var expected = new byte[]
            {
                16, 26, 2, 0, 26, 0
            };

            var actual = new List<byte>();
            foreach (var str in testStrings)
            {
                var digits = str.GetDigits();
                byte summary = 0;
                foreach (var digit in digits)
                {
                    summary += digit;
                }

                actual.Add(summary);
            }

            CollectionAssert.AreEqual(expected, actual, "Expected and Actual results are not equal");
            
        }
    }
}
