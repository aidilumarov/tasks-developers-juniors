﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.ExpressionVisitors.Internal;
using Task_web.Models;

namespace Task_web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IList<TestModel> _testModels;

        public TestController(IList<TestModel> testModels)
        {
            _testModels = testModels;
        }

        // GET /api/Test/TestModels
        [HttpGet]
        [Route("TestModels")]
        public async Task<IActionResult> GetTestModels()
        {
            try
            {
                return Ok(_testModels);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // GET /api/Test/TestModels/[GUID]
        [HttpGet]
        [Route("TestModels/{guid}")]
        public async Task<IActionResult> GetTestModel([FromRoute] Guid guid)
        {
            var testModel = _testModels.SingleOrDefault(t => t.Id == guid);

            if (testModel == null)
                return NotFound();

            return Ok(testModel);
        }

        // POST /api/Test/TestModels
        [HttpPost]
        [Route("TestModels")]
        public async Task<IActionResult> CreateTestModel([FromBody] TestModel model)
        {
            var modelWithSameGuid = await _testModels
                .ToAsyncEnumerable()
                .SingleOrDefault(t => t.Id == model.Id);

            if (modelWithSameGuid != null)
                return BadRequest("Model with this GUID already exists!");

            if (model.Id == new Guid())
                model.Id = Guid.NewGuid();
            
            _testModels.Add(model);
            return Created("TestModels/" + model.Id, model);
        }

        // PUT /api/Test/TestModels/[GUID]
        [HttpPut]
        [Route("TestModels/{guid}")]
        public async Task<IActionResult> CreateTestModel([FromRoute] Guid guid, TestModel model)
        {
            var savedTestModel = await _testModels
                .ToAsyncEnumerable()
                .SingleOrDefault(t => t.Id == guid);

            if (savedTestModel == null)
                return NotFound();

            savedTestModel.Name = model.Name;

            return Ok(savedTestModel);
        }

        // DELETE /api/Test/TestModels/[GUID]
        [HttpDelete]
        [Route("TestModels/{guid}")]
        public async Task<IActionResult> DeleteTestModel([FromRoute] Guid guid)
        {
            var savedTestModel = await _testModels
                .ToAsyncEnumerable()
                .SingleOrDefault(t => t.Id == guid);

            if (savedTestModel == null)
                return NotFound();

            _testModels.Remove(savedTestModel);
            return Ok();
        }
    }
}
